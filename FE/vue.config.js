const path = require('path');  
 module.exports = {  
 configureWebpack: {  
     resolve: {  
       alias: {  

         '@navs': path.resolve(__dirname, 'src/components'),
         '@views': path.resolve(__dirname, 'src/views'),
         '@img': path.resolve(__dirname, 'src/assets/img'),
         
       },  

     },  
     
 },
 
}
