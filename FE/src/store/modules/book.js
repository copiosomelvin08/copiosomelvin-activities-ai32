import axios from 'axios'

const book = {
    namespaced:true,
    state: () => ({

        bookData:[],

    }),
    mutations:{

        SET_BOOK_DATA(state, data)
        {
            state.bookData =  data
        },

    },
    actions:{

        getBookData({commit})
        {
            axios.get('http://127.0.0.1:8000/api/book')
            .then(res => {
                commit('SET_BOOK_DATA' , res.data)
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        },

        addBookData({dispatch},book)
        {
            axios.post('http://127.0.0.1:8000/api/book', book)
            .then(res => {
                dispatch('getBookData')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        },

        updateBookData({dispatch},book)
        {
            axios.put(`http://127.0.0.1:8000/api/dashboard/${book.id}`, book)
            .then(res => {
                dispatch('getBookData')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        },

        deleteBookData({dispatch},book)
        {
            axios.delete(`http://127.0.0.1:8000/api/book/${book.id}`)
            .then( (res) => {
                dispatch('getBookData')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        }

    },
    getters:{
        
        data(state)
        {   
            return state.bookData.reverse()
        }

    }

}

export default book