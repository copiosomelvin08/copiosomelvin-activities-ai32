import axios from 'axios'

const patron = {
    namespaced:true,
    state: () => ({

        patronData:[],

    }),
    mutations:{

        SET_PATRON_DATA(state, data)
        {
            state.patronData =  data
        },

    },
    actions:{

        getPatronData({commit})
        {
            axios.get('http://127.0.0.1:8000/api/patron')
            .then(res => {
                commit('SET_PATRON_DATA' , res.data)
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        },

        addPatronData({dispatch},patron)
        {
            axios.post('http://127.0.0.1:8000/api/patron', patron)
            .then(res => {
                dispatch('getPatronData')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        },

        updatePatronData({dispatch},patron)
        {
            axios.put(`http://127.0.0.1:8000/api/patron/${patron.id}`, patron)
            .then(res => {
                dispatch('getPatronData')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        },

        deletePatronData({dispatch},patron)
        {
            axios.delete(`http://127.0.0.1:8000/api/patron/${patron.id}`)
            .then( (res) => {
                dispatch('getPatronData')
                console.log(res.data)
             })
            .catch(error => {
                console.log(error);
             });
        }

    },
    getters:{
        
        data(state)
        {   
            return state.patronData.reverse()
        }

    }

}

export default patron