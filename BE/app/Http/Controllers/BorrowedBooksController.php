<?php

namespace App\Http\Controllers;

use App\Models\borrowedbook;
use Illuminate\Http\Request;

class BorrowedBooksController extends Controller
{
 
    public function index()
    {
        $BORROWED = Book::all();
        return response()->json([$BORROWED]);
    }


 
    public function store(Request $request)
    {
       
        $BORROWED  = new book();

        $BORROWED->patron_id = $request->input('patron_id');
        $BORROWED->copies = $request->input('copies');
        $BORROWED->book_id = $request->input('book_id');

        $BORROWED->save();
        return response()->json($BORROWED);
    }

 
  
    public function update(Request $request, $id)
    {
        $BORROWED  = book::find($id);

        $BORROWED->patron_id = $request->input('patron_id');
        $BORROWED->copies = $request->input('copies');
        $BORROWED->book_id = $request->input('book_id');

        $BORROWED->update();
        return response()->json($BORROWED);
    }

    public function destroy($id)
    {
        $BORROWED = book::find($id);
        $BORROWED->delete();
        return response()->json($BORROWED);
    }
}
