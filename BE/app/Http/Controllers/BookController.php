<?php

namespace App\Http\Controllers;

use App\Models\book;
use Illuminate\Http\Request;

class BookController extends Controller
{
   
    public function index()
    {
        $BOOKS = Book::all();
        return response()->json([$BOOKS]);
    }


    
    public function store(Request $request)
    {
        $BOOKS  = new book();

        $BOOKS->name = $request->input('name');
        $BOOKS->author = $request->input('author');
        $BOOKS->copies = $request->input('copies');
        $BOOKS->category_id = $request->input('category_id');

        $BOOKS->save();
        return response()->json($BOOKS);
    }



  
    public function update(Request $request, $id)
    {
        $BOOKS  = book::find($id);

        $BOOKS->name = $request->input('name');
        $BOOKS->author = $request->input('author');
        $BOOKS->copies = $request->input('copies');
        $BOOKS->category_id = $request->input('category_id');

        $BOOKS->update();
        return response()->json($BOOKS);
    }

 
    public function destroy($id)
    {
        $BOOKS = book::find($id);
        $BOOKS->delete();
        return response()->json($BOOKS);
    }
}
