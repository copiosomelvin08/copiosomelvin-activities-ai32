<?php

namespace App\Http\Controllers;

use App\Models\patron;
use Illuminate\Http\Request;

class PatronController extends Controller
{
    
    public function index()
    {
        $PATRON = Patron::all();
        return response()->json([$PATRON]);
    }


    public function store(Request $request)
    {
        $PATRON  = new patron();

        $PATRON->last_name = $request->input('last_name');
        $PATRON->first_name = $request->input('first_name');
        $PATRON->middle_name = $request->input('middle_name');
        $PATRON->email = $request->input('email');

        $PATRON->save();
        return response()->json($PATRON);
    }


    public function update(Request $request, $id)
    {
        $PATRON  = patron::find($id);

        $PATRON->last_name = $request->input('last_name');
        $PATRON->first_name = $request->input('first_name');
        $PATRON->middle_name = $request->input('middle_name');
        $PATRON->email = $request->input('email');

        $PATRON->update();
        return response()->json($PATRON);
    }


    public function destroy($id)
    {
        $PATRON = patron::find($id);
        $PATRON->delete();
        return response()->json($PATRON);
    }
}
