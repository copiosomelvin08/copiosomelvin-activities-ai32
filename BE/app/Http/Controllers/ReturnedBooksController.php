<?php

namespace App\Http\Controllers;

use App\Models\returnedbook;
use Illuminate\Http\Request;

class ReturnedBooksController extends Controller
{
    
    public function index()
    {
        $RETURNED = Book::all();
        return response()->json([$RETURNED]);
    }

    public function store(Request $request)
    {
        $RETURNED  = new book();

        $RETURNED->book_id = $request->input('book_id');
        $RETURNED->copies = $request->input('copies');
        $RETURNED->patron_id = $request->input('patron_id');

        $RETURNED->save();
        return response()->json($RETURNED);
    }

   
    
    public function update(Request $request, $id)
    {
        $RETURNED  = book::find($id);

        $RETURNED->book_id = $request->input('book_id');
        $RETURNED->copies = $request->input('copies');
        $RETURNED->patron_id = $request->input('patron_id');

        $RETURNED->update();
        return response()->json($RETURNED);
    }

 
    public function destroy($id)
    {
        $RETURNED = book::find($id);
        $RETURNED->delete();
        return response()->json($RETURNED);
    }
}
