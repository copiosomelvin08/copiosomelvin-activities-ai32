<?php

namespace Database\Seeders;

use App\Models\category;
use Illuminate\Database\Seeder;

class categorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
    $mul_rows= [
    [ 'category' => 'Action'],
    [ 'category' => 'Romance'],
    [ 'category' => 'Programming'],
    [ 'category' => 'Horror'],
    [ 'category' => 'Adventure']
];

    foreach ($mul_rows as $rows) {
    category::create($rows);
        }
    }
}