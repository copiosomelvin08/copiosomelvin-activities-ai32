import Vue from "vue";
import VueRouter from "vue-router";
import Dashboard from "@views/Dashboard.vue";


Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Dashboard",
    component: Dashboard
  },
  {
    path: "/patron",
    name: "Patron",
    component: () =>
    import(/* webpackChunkName: "patron" */ "@views/Patron.vue")
  },
  {
    path: "/book",
    name: "Book",
    component: () =>
    import(/* webpackChunkName: "book" */ "@views/Book.vue")
  },
  {
    path: "/settings",
    name: "Settings",
    component: () =>
    import(/* webpackChunkName: "settings" */ "@views/Settings.vue")
  }
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;
