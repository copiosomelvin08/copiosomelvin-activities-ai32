let barChart = document.getElementById('barChart').getContext('2d');
                let massPopChart = new Chart(barChart, {
    type: 'bar', 
    data:{
        labels:['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June'],
        datasets:[{
            label: 'Borrowed Books',
            data:[
                1000,
                840,
                545,
                800,
                338,
                493
            ],
            backgroundColor: 'yellow',
            bordrWidth: 1,
            borderColor:'#777',
            hoverBorderWidth:3,
            hoverBorderColor:'#000'
        }]
        
    },
});

var pie = document.getElementById('pie').getContext('2d');
var myChart = new Chart(pie, {
    type: 'pie',
    data: {
        labels: ['Comedy', 'Scie-Fi', 'Romance', 'Horror', 'Action'],
        datasets: [{
            label: '# of Votes',
            data: [1000, 638, 855, 337, 523],
            backgroundColor: [
                'rgba(255, 99, 132, 0.6)',
                'rgba(255, 159, 64, 0.6)',
                'rgba(153, 102, 255, 0.6)',
                'rgba(75, 192, 192, 0.6)',
                'rgba(54, 111, 235, 0.6)'
                
            ],
        }]
    },
    options:{
        legend:{
            position: 'right'
        }
    }
});




