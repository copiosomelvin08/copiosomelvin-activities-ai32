var bookmodal = document.getElementById('bookModal');

var modalBtn = document.getElementById('button');
var closeBtn = document.getElementsByClassName('closeBtn-books')[0];

modalBtn.addEventListener('click', openModal);
closeBtn.addEventListener('click', closeModal);
window.addEventListener('click', outsideClick);

function openModal()
{
    bookmodal.style.display = 'block';
}
function closeModal()
{
    bookmodal.style.display ='none';

    document.getElementById("Name").value = "";
    document.getElementById("author").value = "";
    document.getElementById("copies").value = "";
    document.getElementById("category").value = "";
}
function outsideClick(e)
{
    if(e.target == bookmodal)
    {
    bookmodal.style.display ='none';

    document.getElementById("Name").value = "";
    document.getElementById("author").value = "";
    document.getElementById("copies").value = "";
    document.getElementById("category").value = "";
    }
}

function onFormSubmit()
{
    bookmodal.style.display = 'block';
    var formData = readFormData();
    insertNewRecord(formData);
    bookmodal.style.display = 'none';
    $("#bookModal").modal("hide");

    document.getElementById("Name").value = "";
    document.getElementById("author").value = "";
    document.getElementById("copies").value = "";
    document.getElementById("category").value = "";
}

function readFormData()
{
    var formData = {};
    formData["Name"] = document.getElementById("Name").value;
    formData["author"] = document.getElementById("author").value;
    formData["copies"] = document.getElementById("copies").value;
    formData["category"] = document.getElementById("category").value;
    return formData;
}

function insertNewRecord(data)
{
    var table = document.getElementById("books").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.Name;
    cell2 = newRow.insertCell(1);
    cell2.innerHTML = data.author;
    cell3 = newRow.insertCell(2);
    cell3.innerHTML = data.copies;
    cell4 = newRow.insertCell(3);
    cell4.innerHTML = data.category;
    cell4 = newRow.insertCell(4);
    cell4.innerHTML = `<a onClick="onEdit(this)"><img src="assets/img/edit.png" width="36px" height"33px"></img></a>
                       <a onClick="onDelete(this)"><img src="assets/img/delete.png" width="36px" height"33px"></img></a>`;
}

function onEdit(td)
{   
    bookmodal.style.display = 'block';
    selectedRow = td.parentElement.parentElement;
    document.getElementById("Name").value = selectedRow.cells[0].innerHTML;
    document.getElementById("author").value = selectedRow.cells[1].innerHTML;
    document.getElementById("copies").value = selectedRow.cells[2].innerHTML;
    document.getElementById("category").value = selectedRow.cells[3].innerHTML;

    row = td.parentElement.parentElement;
    document.getElementById("books").deleteRow(row.rowIndex);
}

function onDelete(td)
{
    if(confirm('Are you sure you want to delete this record?')){
    row = td.parentElement.parentElement;
    document.getElementById("books").deleteRow(row.rowIndex);

    document.getElementById("Name").value = "";
    document.getElementById("author").value = "";
    document.getElementById("copies").value = "";
    document.getElementById("category").value = "";
    }
}