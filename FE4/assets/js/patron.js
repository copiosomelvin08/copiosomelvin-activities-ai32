var patronmodal = document.getElementById('patronModal');

var modalBtn = document.getElementById('button');
var closeBtn = document.getElementsByClassName('closeBtn-patron')[0];

modalBtn.addEventListener('click', openModal);
closeBtn.addEventListener('click', closeModal);
window.addEventListener('click', outsideClick);

function openModal()
{
    patronmodal.style.display = 'block';

    document.getElementById("firstName").value = "";
    document.getElementById("middleName").value = "";
    document.getElementById("lastName").value = "";
    document.getElementById("emailAdd").value = "";
}
function closeModal()
{
    patronmodal.style.display ='none';

    document.getElementById("firstName").value = "";
    document.getElementById("middleName").value = "";
    document.getElementById("lastName").value = "";
    document.getElementById("emailAdd").value = "";
}
function outsideClick(e)
{
    if(e.target == patronmodal)
    {
    patronmodal.style.display ='none';

    document.getElementById("firstName").value = "";
    document.getElementById("middleName").value = "";
    document.getElementById("lastName").value = "";
    document.getElementById("emailAdd").value = "";
    }
}


function onFormSubmit()
{
    var formData = readFormData();
    insertNewRecord(formData);
    patronmodal.style.display = 'none';

    document.getElementById("firstName").value = "";
    document.getElementById("middleName").value = "";
    document.getElementById("lastName").value = "";
    document.getElementById("emailAddress").value = "";
}

function readFormData()
{
    var formData = {};
    formData["firstName"] = document.getElementById("firstName").value;
    formData["middleName"] = document.getElementById("middleName").value;
    formData["lastName"] = document.getElementById("lastName").value;
    formData["emailAdd"] = document.getElementById("emailAdd").value;
    return formData;
}

function insertNewRecord(data)
{
    var table = document.getElementById("patron").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.firstName
    cell2 = newRow.insertCell(1);
    cell2.innerHTML = data.emailAdd;
    cell3 = newRow.insertCell(2);
    cell3.innerHTML = data.lastName;
    cell3 = newRow.insertCell(3);
    cell3.innerHTML = `<a onClick="onEdit(this)"><img src="assets/img/edit.png" width="36px" height"33px"></img></a>
                       <a onClick="onDelete(this)"><img src="assets/img/delete.png" width="36px" height"33px"></img></a>`;
                       
}

function onEdit(td)
{
    patronmodal.style.display = 'block';
    selectedRow = td.parentElement.parentElement;
    document.getElementById("firstName").value = selectedRow.cells[0].innerHTML;
    document.getElementById("lastName").value = selectedRow.cells[2].innerHTML;
    document.getElementById("emailAdd").value = selectedRow.cells[1].innerHTML;

    row = td.parentElement.parentElement;
    document.getElementById("books").deleteRow(row.rowIndex);
}

function onDelete(td)
{
    if(confirm('Are you sure you want to delete this record?')){
    row = td.parentElement.parentElement;
    document.getElementById("patron").deleteRow(row.rowIndex);

    document.getElementById("firstName").value = "";
    document.getElementById("middleName").value = "";
    document.getElementById("lastName").value = "";
    document.getElementById("emailAdd").value = "";
    }
}